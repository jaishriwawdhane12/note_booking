package com.appyhigh;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by jaishri on 25/2/19.
 */

public class Constants {
    public static final String ISFIRSTTIME = "isFirstTime?";
    public static Constants instance;
    private static Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    private Constants(Context context) {
        this.context = context;
        sharedPref = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        editor = sharedPref.edit();
    }

    public static Constants getInstance(Context context) {
        if (instance == null)
            instance = new Constants(context);
        return instance;
    }

    public boolean isFirstTime() {
        return sharedPref.getBoolean(ISFIRSTTIME, true);
    }

    public void setFirstTime(boolean value) {
        editor.putBoolean(ISFIRSTTIME, value);
        editor.commit();
    }
}
