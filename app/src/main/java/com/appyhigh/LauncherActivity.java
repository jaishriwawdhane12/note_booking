package com.appyhigh;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.appyhigh.seatbooking.view.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by jaishri on 24/2/19.
 */

public class LauncherActivity extends AppCompatActivity {

    @BindView(R.id.btnNoteActivity)
    Button btnNoteActivity;
    @BindView(R.id.btnSeatActivity)
    Button btnSeatActivity;
    private Unbinder unbinder;

    @OnClick(R.id.btnSeatActivity)
    void toSeatActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @OnClick(R.id.btnNoteActivity)
    void toNoteActivity() {
        startActivity(new Intent(this, com.appyhigh.notes.view.MainActivity.class));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        unbinder = ButterKnife.bind(this);
    }
}
