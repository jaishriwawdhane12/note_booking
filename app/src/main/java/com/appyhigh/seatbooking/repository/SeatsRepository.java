package com.appyhigh.seatbooking.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.appyhigh.seatbooking.roomdb.Seats;
import com.appyhigh.seatbooking.roomdb.SeatsDao;
import com.appyhigh.seatbooking.roomdb.SeatsDatabAse;

import java.util.List;

/**
 * Created by jaishri on 24/2/19.
 */

public class SeatsRepository {
    private SeatsDao seatsDao;
    private Seats seats;
    private LiveData<List<Seats>> allSeats;
    private int i = 0;

    public SeatsRepository(Application application) {
        SeatsDatabAse databAse = SeatsDatabAse.getInstance(application);
        seatsDao = databAse.seatsDao();
        allSeats = seatsDao.getAllSetas();
    }

    public Seats getASeat(int position) {
        i = 4;
        new processSeatsTask().execute();
        return seats;
    }


    public void insert(Seats seats) {
        i = 0;
        new processSeatsTask().execute(seats);

    }

    public void update(Seats seats) {

        i = 1;
        new processSeatsTask().execute(seats);
    }

    public void delete(Seats seats) {

        i = 2;
        new processSeatsTask().execute(seats);
    }

    public void deleteAllSeats() {

        i = 3;
        new processSeatsTask().execute();
    }

    public LiveData<List<Seats>> getAllSeats() {
        return allSeats;
    }

    private class processSeatsTask extends AsyncTask<Seats, Void, Void> {

        @Override
        protected Void doInBackground(Seats... seats) {

            if (i == 0)
                seatsDao.inset(seats[0]);
            else if (i == 1)
                seatsDao.update(seats[0]);
            else if (i == 2)
                seatsDao.delete(seats[0]);
            else if (i == 3)
                seatsDao.deleteAllSeats();
            else if (i == 4)
                seats[0] = seatsDao.getASeat(seats[0].getSeatNo());
            return null;
        }
    }
}
