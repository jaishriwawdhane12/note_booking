package com.appyhigh.seatbooking.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appyhigh.R;
import com.appyhigh.seatbooking.roomdb.Seats;
import com.appyhigh.seatbooking.viewmodel.SeatViewModel;

import java.util.List;

public class ImageAdapter extends BaseAdapter {
    // Keep all Images in array
    private static final String TAG = "ImageAdapter";

    private Context mContext;
    private LayoutInflater layoutInflater;
    private List<Integer> chairs;

    private SeatViewModel seatViewModel;
    private List<Seats> seats;
    private int i = 0;
    private int bookedSeats = 0;

    private int position;

    // Constructor
    public ImageAdapter(Context c, List<Seats> seats, List<Integer> chairs, SeatViewModel seatViewModel) {
        this.seats = seats;
        mContext = c;
        this.chairs = chairs;
        this.seatViewModel = seatViewModel;
        layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    public int getCount() {
        return chairs.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        this.position = position;
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.grid_row_tem, parent, false);
            viewHolder.image = convertView.findViewById(R.id.imgChair);
            viewHolder.seatNo = convertView.findViewById(R.id.seatNo);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.image.setImageResource(chairs.get(position));
        viewHolder.seatNo.setText(String.valueOf(position));

        if (position > 18 && position < 21 || position > 26 && position < 29 || position > 34 && position < 37) {
            viewHolder.image.setVisibility(View.INVISIBLE);
            viewHolder.seatNo.setVisibility(View.INVISIBLE);
        }
        if (position < 40) {
            try {
                if (seats.get(position).isBooked()) {
                    viewHolder.image.setImageResource(R.drawable.seat_normal_booked);
                    bookedSeats++;

                } else {
                    viewHolder.image.setImageResource(R.drawable.seat_normal);
                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
                viewHolder.image.setImageResource(R.drawable.seat_normal);

            }
        } else {
            viewHolder.image.setVisibility(View.GONE);
            viewHolder.seatNo.setVisibility(View.GONE);
            viewHolder.image.setClickable(false);
            viewHolder.seatNo.setClickable(false);

        }

        final ViewHolder finalViewHolder = viewHolder;
        viewHolder.image.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View view) {

                Log.d(TAG, bookedSeats + "");
                if (!seatViewModel.getAllSeats().getValue().get(position).isBooked()) {
                    if (i < (33)) {
                        finalViewHolder.image.setImageResource(R.drawable.seat_normal_selected);
                        seatViewModel.getAllSeats().getValue().get(position).setSeatNo(position);
                        seatViewModel.getAllSeats().getValue().get(position).setAvailable(false);
                        seatViewModel.getAllSeats().getValue().get(position).setBooked(true);
                        seatViewModel.getAllSeats().getValue().get(position).setXx(false);
                        i++;
                    } else {
                        Toast.makeText(mContext, "You can book only " + i + " Seats.Thank you", Toast.LENGTH_SHORT).show();
                    }

                } else if (seatViewModel.getAllSeats().getValue().get(position).isBooked() &&
                        seatViewModel.getAllSeats().getValue().get(position).isXx()) {

                } else {
                    finalViewHolder.image.setImageResource(R.drawable.seat_normal);
                    seatViewModel.getAllSeats().getValue().get(position).setSeatNo(position);
                    seatViewModel.getAllSeats().getValue().get(position).setAvailable(true);
                    seatViewModel.getAllSeats().getValue().get(position).setBooked(false);
                    i--;
                }
                Log.d(TAG, i + "");
            }
        });
        return convertView;
    }

    public void setSeats(List<Seats> seats) {
        this.seats = seats;
        notifyDataSetChanged();
    }

    public Seats getSeatAt(int position) {
        return seats.get(position);
    }


    private class ViewHolder {
        public ImageView image;
        public TextView seatNo;
    }
}