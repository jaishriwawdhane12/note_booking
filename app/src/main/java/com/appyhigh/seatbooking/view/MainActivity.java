package com.appyhigh.seatbooking.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.appyhigh.Constants;
import com.appyhigh.R;
import com.appyhigh.seatbooking.adapter.ImageAdapter;
import com.appyhigh.seatbooking.roomdb.Seats;
import com.appyhigh.seatbooking.viewmodel.SeatViewModel;

import java.util.List;
import java.util.Vector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.Unbinder;

/**
 * Created by jaishri on 24/2/19.
 */

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "MainActivity";
    @BindView(R.id.gridView)
    GridView gridView;
    @BindView(R.id.btnBookSeat)
    Button btnBookSeat;
    private SeatViewModel seatViewModel;
    private Unbinder unbinder;
    private ImageAdapter imageAdapter;
    private List<Integer> chairs;
    private List<Seats> seatsList;
    private Seats seats;
    private List<Integer> updatedPositions = new Vector<>();

    private int i = 0;
    private int position;

    @OnClick(R.id.btnBookSeat)
    void clickSeatBooking() {

        for (int j = 0; j < seatsList.size(); j++) {
            imageAdapter.getSeatAt(j).setXx(true);
            seatViewModel.update(imageAdapter.getSeatAt(j));
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_seat);

        unbinder = ButterKnife.bind(this);
        chairs = new Vector<>();
        seatsList = new Vector<>();
        seatViewModel = ViewModelProviders.of(this).get(SeatViewModel.class);


        Log.d(TAG, "Total: " + seatsList.size());


        try {
            if (Constants.getInstance(this).isFirstTime()) {
                for (int j = 0; j < 40; j++) {
                    seats = new Seats();
                    seats.setSeatNo(j);
                    seats.setBooked(false);
                    seats.setAvailable(true);
                    seatsList.add(seats);
                    chairs.add(R.drawable.seat_normal);

                    seatViewModel.insert(seats);

                    if (seatsList.get(j).isBooked()) {
                        chairs.add(R.drawable.seat_normal_booked);

                    } else {
                        chairs.add(R.drawable.seat_normal);
                    }
                }
                Constants.getInstance(this).setFirstTime(false);

            } else {
                for (int j = 0; j < 40; j++) {
                    seats = new Seats();
                    seats.setSeatNo(j);
                    seats.setBooked(false);
                    seats.setAvailable(true);
                    seatsList.add(seats);
                    chairs.add(R.drawable.seat_normal);

                    seatViewModel.update(seats);

                    if (seatsList.get(j).isBooked()) {
                        chairs.add(R.drawable.seat_normal_booked);

                    } else {
                        chairs.add(R.drawable.seat_normal);
                    }

                }
            }
        } catch (ArrayIndexOutOfBoundsException ae) {
            ae.printStackTrace();
        }

        imageAdapter = new ImageAdapter(this, seatsList, chairs, seatViewModel);
        gridView.setAdapter(imageAdapter);
        seatViewModel.getAllSeats().observe(this, new Observer<List<Seats>>() {
            @Override
            public void onChanged(@Nullable List<Seats> seats) {
                Log.d(TAG, "Observe" + seats.size());
                imageAdapter.setSeats(seats);
            }
        });
        updatedPositions.clear();

    }

    @OnItemClick(R.id.gridView)
    void onChairClick(AdapterView<?> adapterView, View imgView, int position, long l) {
    }


}