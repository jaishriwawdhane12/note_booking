package com.appyhigh.seatbooking.roomdb;

import android.arch.persistence.room.PrimaryKey;

/**
 * Created by jaishri on 24/2/19.
 */
@android.arch.persistence.room.Entity(tableName = "seats_table")
public class Seats {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private int seatNo;
    private boolean isBooked;
    private boolean isAvailable;

    public boolean isXx() {
        return xx;
    }

    public void setXx(boolean xx) {
        this.xx = xx;
    }

    private boolean xx;



    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isBooked() {
        return isBooked;
    }

    public void setBooked(boolean booked) {
        isBooked = booked;
    }

    public int getSeatNo() {
        return seatNo;
    }

    public void setSeatNo(int seatNo) {
        this.seatNo = seatNo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
