package com.appyhigh.seatbooking.roomdb;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by jaishri on 24/2/19.
 */
@Database(entities = {Seats.class}, version = 1)
public abstract class SeatsDatabAse extends RoomDatabase {
    private static SeatsDatabAse instance;

    public static synchronized SeatsDatabAse getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    SeatsDatabAse.class, "seats.db")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract SeatsDao seatsDao();
}
