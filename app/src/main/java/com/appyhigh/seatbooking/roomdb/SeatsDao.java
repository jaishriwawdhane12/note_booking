package com.appyhigh.seatbooking.roomdb;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by jaishri on 24/2/19.
 */
@Dao
public interface SeatsDao {
    @Insert
    void inset(Seats seats);

    @Update
    void update(Seats seats);

    @Delete
    void delete(Seats seats);

    @Query("Delete from seats_table")
    void deleteAllSeats();

    @Query("Select * from seats_table where seatNo = :position")
    Seats getASeat(int position);


    @Query("Select * from seats_table order by id")
    LiveData<List<Seats>> getAllSetas();


}
