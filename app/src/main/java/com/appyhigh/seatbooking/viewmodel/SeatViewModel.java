package com.appyhigh.seatbooking.viewmodel;

import android.app.Application;
import android.app.ListActivity;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.appyhigh.seatbooking.repository.SeatsRepository;
import com.appyhigh.seatbooking.roomdb.Seats;
import com.appyhigh.seatbooking.roomdb.SeatsDao;
import com.appyhigh.seatbooking.roomdb.SeatsDatabAse;

import java.util.List;

/**
 * Created by jaishri on 24/2/19.
 */

public class SeatViewModel extends AndroidViewModel {
    private SeatsRepository seatsRepository;
    private LiveData<List<Seats>> allSeats;
    public SeatViewModel(@NonNull Application application) {
        super(application);
        seatsRepository = new SeatsRepository(application);
        allSeats = seatsRepository.getAllSeats();
    }

    public Seats getASeat(int seatNo){
        return seatsRepository.getASeat(seatNo);
    }

    public void insert(Seats seats){
        seatsRepository.insert(seats);
    }

    public void update(Seats seats){
        seatsRepository.update(seats);
    }

    public void delete(Seats seats){
        seatsRepository.delete(seats);
    }

    public void deleteAll(){
        seatsRepository.deleteAllSeats();
    }

    public LiveData<List<Seats>> getAllSeats(){
        return allSeats;
    }
}
