package com.appyhigh.notes.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appyhigh.R;
import com.appyhigh.notes.roomdb.Notes;
import com.appyhigh.notes.viewmodel.NoteViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jaishri on 23/2/19.
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteHolder> {
    private static final String TAG = "NoteAdapter";
    private List<Notes> notes = new ArrayList<>();
    private OnItemClickListener listener;
    private NoteViewModel noteViewModel;
    private Context context;

    private NoteHolder holder;
    private int position;
    private Notes currentNote;

    public NoteAdapter(Context context, NoteViewModel noteViewModel) {
        this.context = context;
        this.noteViewModel = noteViewModel;
    }

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_item, parent, false);
        return new NoteHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder holder, int position) {
        this.position = position;
        currentNote = notes.get(position);
        holder.textViewTitle.setText(currentNote.getTitle());
        holder.textViewDescription.setText(currentNote.getDescription());
        holder.textViewPriority.setText(String.valueOf(currentNote.getPriority()));
        this.holder = holder;
        holder.edtViewTitle.setText(currentNote.getTitle());
        holder.edtViewDescription.setText(currentNote.getDescription());
        holder.edtViewPriority.setText(String.valueOf(currentNote.getPriority()));
        holder.rlShow.setVisibility(View.VISIBLE);
        holder.rlEdit.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public void setNotes(List<Notes> notes) {
        this.notes = notes;
        notifyItemChanged(position);
        notifyDataSetChanged();

    }

    public Notes getNoteAt(int position) {
        return notes.get(position);
    }

    private void updateListData(int position, Notes note) {
        notes.remove(position);
        notes.add(position, note);
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    public interface OnItemClickListener {
        void onItemClick(Notes note);
    }


    class NoteHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rl_show)
        RelativeLayout rlShow;
        @BindView(R.id.text_view_title)
        TextView textViewTitle;
        @BindView(R.id.text_view_description)
        TextView textViewDescription;
        @BindView(R.id.text_view_priority)
        TextView textViewPriority;

        @BindView(R.id.rl_update)
        RelativeLayout rlEdit;
        @BindView(R.id.edt_view_title)
        EditText edtViewTitle;
        @BindView(R.id.edt_view_description)
        EditText edtViewDescription;
        @BindView(R.id.edt_view_priority)
        EditText edtViewPriority;
        @BindView(R.id.btnUpdate)
        Button btnUpdate;
        @BindView(R.id.btnDelete)
        Button btnDelete;

        public NoteHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(notes.get(position));
                        currentNote = getNoteAt(position);
                        rlShow.setVisibility(View.GONE);
                        rlEdit.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

        @OnClick(R.id.btnDelete)
        void deleteRow() {
            noteViewModel.delete(currentNote);
            notifyItemChanged(position);
            notifyDataSetChanged();
            rlEdit.setVisibility(View.GONE);
            rlShow.setVisibility(View.VISIBLE);
        }

        @OnClick(R.id.btnUpdate)
        void onUpdateClick() {
            String title = edtViewTitle.getText().toString();
            String description = edtViewDescription.getText().toString();
            String priority = edtViewPriority.getText().toString();
            if (title.equals(""))
                Toast.makeText(context, "Please enter title", Toast.LENGTH_SHORT).show();
            else if (description.equals(""))
                Toast.makeText(context, "Please enter description", Toast.LENGTH_SHORT).show();
            else if (priority.equals(""))
                Toast.makeText(context, "Please enter priority", Toast.LENGTH_SHORT).show();
            else {
                Log.d(TAG, "Update" + position + ", " + notes.get(position).getId());
                currentNote.setDescription(description);
                currentNote.setTitle(title);
                currentNote.setPriority(Integer.parseInt(priority));
                currentNote.setId(notes.get(position).getId());
                updateListData(position, currentNote);
                noteViewModel.update(currentNote);
                rlEdit.setVisibility(View.GONE);
                rlShow.setVisibility(View.VISIBLE);

                textViewDescription.setText(currentNote.getDescription());
                textViewTitle.setText(currentNote.getTitle());
                textViewPriority.setText(String.valueOf(currentNote.getPriority()));
                notifyItemChanged(position);
                notifyDataSetChanged();
            }
        }
    }
}