package com.appyhigh.notes.roomdb;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;

import java.util.List;

/**
 * Created by jaishri on 23/2/19.
 */
@Dao
public interface NoteDao {
    @android.arch.persistence.room.Insert
    void insert(Notes note);

    @android.arch.persistence.room.Update
    void update(Notes note);

    @Delete
    void delete(Notes note);

    @android.arch.persistence.room.Query("DELETE FROM note_table")
    void deleteAllNotes();

    @android.arch.persistence.room.Query("SELECT * FROM note_table ORDER BY id DESC")
    android.arch.lifecycle.LiveData<List<Notes>> getAllNotes();
}

