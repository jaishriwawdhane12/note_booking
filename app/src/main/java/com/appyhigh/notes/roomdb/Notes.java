package com.appyhigh.notes.roomdb;

import android.arch.persistence.room.PrimaryKey;

/**
 * Created by jaishri on 23/2/19.
 */

@android.arch.persistence.room.Entity(tableName = "note_table")
public class Notes {
    @PrimaryKey(autoGenerate = true)
    private int id;

    private String title;

    private String description;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    private int priority;

    public Notes(String title, String description, int priority) {
        this.title = title;
        this.description = description;
        this.priority = priority;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getPriority() {
        return priority;
    }
}
