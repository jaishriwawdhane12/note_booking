package com.appyhigh.notes.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.appyhigh.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by jaishri on 23/2/19.
 */

public class AddEditNoteActivity extends AppCompatActivity {
    public static final String EXTRA_ID =
            "com.com.appyhigh.notes.EXTRA_ID";
    public static final String EXTRA_TITLE =
            "com.com.appyhigh.notes.EXTRA_TITLE";
    public static final String EXTRA_DESCRIPTION =
            "com.com.appyhigh.notes.EXTRA_DESCRIPTION";
    public static final String EXTRA_PRIORITY =
            "com.com.appyhigh.notes.EXTRA_PRIORITY";

    private Unbinder unbinder;
    @BindView(R.id.edit_text_title)
    EditText editTextTitle;
    @BindView(R.id.edit_text_description)
    EditText editTextDescription;
    @BindView(R.id.number_picker_priority)
    NumberPicker numberPickerPriority;

    @BindView(R.id.btnAddNote)
    Button btnAddNote;

    @OnClick(R.id.btnAddNote)
    void onClick(){
        saveNote();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(unbinder!=null)
            unbinder.unbind();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);
        unbinder = ButterKnife.bind(this);
        numberPickerPriority.setMinValue(1);
        numberPickerPriority.setMaxValue(10);

        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.plus);

        Intent intent = getIntent();

        if (intent.hasExtra(EXTRA_ID)) {
            setTitle("Edit Note");
            editTextTitle.setText(intent.getStringExtra(EXTRA_TITLE));
            editTextDescription.setText(intent.getStringExtra(EXTRA_DESCRIPTION));
            numberPickerPriority.setValue(1);
        } else {
            setTitle("Add Note");
        }
    }

    private void saveNote() {
        String title = editTextTitle.getText().toString();
        String description = editTextDescription.getText().toString();
        int priority = numberPickerPriority.getValue();

        if (title.trim().isEmpty() || description.trim().isEmpty()) {
            Toast.makeText(this, "Please insert a title and description", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_TITLE, title);
        data.putExtra(EXTRA_DESCRIPTION, description);
        data.putExtra(EXTRA_PRIORITY, priority);

        int id = getIntent().getIntExtra(EXTRA_ID, -1);
        if (id != -1) {
            data.putExtra(EXTRA_ID, id);
        }

        setResult(RESULT_OK, data);
        finish();
    }
}