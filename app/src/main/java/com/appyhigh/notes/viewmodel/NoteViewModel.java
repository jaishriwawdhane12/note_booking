package com.appyhigh.notes.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.appyhigh.notes.repositoy.NoteRepository;
import com.appyhigh.notes.roomdb.Notes;

import java.util.List;

/**
 * Created by jaishri on 23/2/19.
 */

public class NoteViewModel extends AndroidViewModel {
    private NoteRepository repository;
    private android.arch.lifecycle.LiveData<List<Notes>> allNotes;

    public NoteViewModel(@NonNull Application application) {
        super(application);
        repository = new NoteRepository(application);
        allNotes = repository.getAllNotes();
    }

    public void insert(Notes notes) {
        repository.insert(notes);
    }

    public void update(Notes notes) {
        repository.update(notes);
    }

    public void delete(Notes notes) {
        repository.delete(notes);
    }

    public void deleteAllNotes() {
        repository.deleteAllNotes();
    }

    public android.arch.lifecycle.LiveData<List<Notes>> getAllNotes() {
        return allNotes;
    }
}